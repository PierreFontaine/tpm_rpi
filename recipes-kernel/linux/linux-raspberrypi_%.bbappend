FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

PACKAGE_ARCH = "raspberrypi3"

SRC_URI += "file://letstrust-tpm-overlay.dts;subdir=git/arch/${ARCH}/boot/dts/overlays"

KERNEL_DEVICETREE += "overlays/letstrust-tpm.dtbo"